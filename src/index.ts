import { CountDuplicates } from "./count-duplicates";

const result = new CountDuplicates('teste').calculate();
expect(result.t).toBe(2);
