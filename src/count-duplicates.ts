import { Result } from './types';

export class CountDuplicates {
  private sentence: string;

  constructor(sentence: string) {
    this.sentence = sentence;
  }

  public calculate(): Result {

    const result = this.sentence.split('').reduce((result: Result, word: string): Result => {
      if (result[word]) {
        result[word] += 1;
        return result;
      }

      result[word] = 1;

      return result;
    }, {});

    return this.removeNonDuplicates(result);

  }

  private removeNonDuplicates(result: Result) : Result{

    return Object.keys(result).reduce((r: Result, t: string): Result => {
      if (result[t] > 1) {
        r[t] = result[t];
      } return r;
    }, {});

  }

}
