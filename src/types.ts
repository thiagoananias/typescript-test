export type BaseResult = {
  key: number;
};

export type Result = {
  [key: string]: number;
};
