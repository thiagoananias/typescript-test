import { CountDuplicates } from '../src/count-duplicates';

describe('Count duplicates spec', () => {

  it('Should have two duplicates', () => {
    const result = new CountDuplicates('teste').calculate();
    expect(result.t).toBe(2);
    expect(result.s).toBe(undefined);
  });

  it('Should not have duplicates', () => {
    const result = new CountDuplicates('abcdefgh').calculate();
    expect(result).toStrictEqual({});
  });

});
